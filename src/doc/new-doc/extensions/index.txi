@node Extensions
@chapter Extensions

@menu
* System building::
* Operating System Interface::
* Foreign Function Interface::
* Multithreading::
* Signals and Interrupts::
@c Networking::
* Memory Management::
* Meta-Object Protocol (MOP)::
@c * Green Threads::
@c * Continuations::
@c * Extensible Sequences::
* Gray Streams::
@c * Series::
* Tree walker::
@c * Local package nicknames::
* CDR Extensions::
@end menu

@node System building
@section System building

@node Operating System Interface
@section Operating System Interface

@include extensions/ffi.txi

@node Multithreading
@section Multithreading

@node Signals and Interrupts
@section Signals and Interrupts

@node Memory Management
@section Memory Management

@node Meta-Object Protocol (MOP)
@section Meta-Object Protocol (MOP)

@c @node Green Threads
@c @section Green Threads

@c @node Continuations
@c @section Continuations

@c @node Extensible Sequences
@c @section Extensible Sequences

@node Gray Streams
@section Gray Streams

@c @node Series
@c @section Series

@node Tree walker
@section Tree walker

@c @node Local package nicknames
@c @section Local package nicknames

@node CDR Extensions
@section CDR Extensions
